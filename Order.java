public class Order {
	
	public static final int MAX_NUMBERS_ORDERED = 3;
	private DigitalVideoDisc itemsOrdered[] = new DigitalVideoDisc[MAX_NUMBERS_ORDERED];
	private int qtyOrdered;
	private MyDate dateOrdered;
	public static int nbOrders=0;
	public static final int MAX_LIMITTED_ORDERS=3;
	
	public int getQtyOrdered() {
		return qtyOrdered;
	}
	
	public void setQtyOrdered(int qtyOrdered) {
		this.qtyOrdered = qtyOrdered;
	}
	
	public MyDate getDateOrdered() {
		return dateOrdered;
	}
	
	public void setDateOrdered(int day, int month, int year) {
		dateOrdered.setDay(day);
		dateOrdered.setMonth(month);
		dateOrdered.setYear(year);
	}
	
	public void setDateOrdered(String date) {
		dateOrdered.trans(date);
	}
	
	public void addDigitalVideoDisc(DigitalVideoDisc disc) {
		if(this.qtyOrdered == MAX_NUMBERS_ORDERED) {
			System.out.println("full -> ko the add them "+ disc.getTitle());
			return;
		}else {
			this.itemsOrdered[this.qtyOrdered++] = disc;
			System.out.println("add thanh cong "+ disc.getTitle());
		}
	}
	
	public void addDigitalVideoDisc(DigitalVideoDisc[] dvdList) {
		for(int i=0; i<dvdList.length; i++) {
			if(this.qtyOrdered == MAX_NUMBERS_ORDERED) {
				System.out.println("full -> ko the add them "+dvdList[i].getTitle());
				return;
			}
			addDigitalVideoDisc(dvdList[i]);
		}
	}

	public void addDigitalVideoDisc(DigitalVideoDisc dvd1, DigitalVideoDisc dvd2) {
		addDigitalVideoDisc(dvd1);
		addDigitalVideoDisc(dvd2);
	}
	
	public void removeDigitalVideoDisc(DigitalVideoDisc disc) {
		for(int i=0; i<qtyOrdered; i++) {
			if(this.itemsOrdered[i] == disc) {
				for(int j=i; j < qtyOrdered-1; j++) {
						this.itemsOrdered[j] = this.itemsOrdered[j+1];
			}
			this.qtyOrdered-=1;
			System.out.println("remove thanh cong "+disc.getTitle());
			return;
			}
		}
		System.out.println("remove error");
	}
	
	public float totalCost() {
		float total = 0;
		for(int i=0; i<this.qtyOrdered; i++) {
			total += this.itemsOrdered[i].getCost();
		}
		return total;
	}
	
	public void printOrdered() {
		System.out.println("********************Order********************");
		System.out.println("Date: "+dateOrdered.getDay()+"-"+dateOrdered.getMonth()+"-"+dateOrdered.getYear());
		System.out.println("Ordered Items: ");
		for(int i=0; i<qtyOrdered; i++) {
			System.out.print((i+1)+". DVD");
			System.out.print(" - " + itemsOrdered[i].getTitle());
			System.out.print(" - " + itemsOrdered[i].getCategory());
			System.out.print(" - " + itemsOrdered[i].getDirector()); 
			System.out.print(" - " + itemsOrdered[i].getLength());
			System.out.print(": " + itemsOrdered[i].getCost()+"$\n");
		}
		System.out.println("Total cost: "+totalCost()+"$");
		System.out.println("*********************************************");
	}
	
	public Order() {
		if(this.nbOrders >= MAX_LIMITTED_ORDERS) {
			System.out.println("So luong don hang vuot qua cho phep!");
			return;
		}
		this.qtyOrdered = 0;
		dateOrdered = new MyDate();
		nbOrders++;
	}
}