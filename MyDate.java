import java.util.Calendar;
import java.util.Scanner;
public class MyDate {
	
	private int day;
	private int month;
	private int year;
	Calendar cal = Calendar.getInstance();
	
	public MyDate() {
		this.day = cal.get(Calendar.DAY_OF_MONTH);
		this.month = cal.get(Calendar.MONTH)+1;
		this.year = cal.get(Calendar.YEAR);
	}
	
	public MyDate(int day, int month, int year) {
		this.day = day;
		this.month = month;
		this.year = year;
	}

	public MyDate(String date) {
		trans(date);
	}

	MyDate(String day,  String  month, String year) {
		String dayArr[] = {"first", "second","third","fourth","fifth","sixth","seventh","eighth","nineth","tenth","eleventh","twelfth","thirdteem", "fourteenth","fifteenth", "sixteenth", "seventeenth", "eighteenth", "nineteenth", "twentieth", "twenty-first", "twenty-second", "twenty-third", "twenty-fourth", "twenty-fifth", "twenty-sixth", "twenty-seventh", "twenty-eighth", "twenty-ninth", "thirtieth", "thirty-first"};
		day = day.toLowerCase();
		for(int i=0; i<31; i++) {
	    	if(day.equals(dayArr[i])) {
	    		this.day=i+1;
	    		break;
	    	}
	    	if(i==30) {
	    		System.out.println("Ngay nhap vao khong dung!");
	    	}
	    }
		
		String monthArr[] = {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
		for(int i=0; i<12; i++) {
	    	if(month.equals(monthArr[i])) {
	    		this.month=i+1;
	    		break;
	    	}
	    	if(i==11) {
	    		System.out.println("Thang nhap vao khong dung!");
	    	}
	    }
		
		String unitsArray[] = { "zero", "one", "two", "three", "four", "five", "six", 
                "seven", "eight", "nine", "ten", "eleven", "twelve",
                "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", 
                "eighteen", "nineteen" };
		String tensArray[] = { "zero", "ten", "twenty", "thirty", "forty", "fifty",
                "sixty", "seventy", "eighty", "ninety" };
		String[] yearList = year.split("\\s");
		int yNumber = 0;
		switch (yearList.length) {
		case 4:
	          for(int i=0;i<10;i++) if(tensArray[i].equals(yearList[0])) yNumber += i*1000;
	          for(int i=0;i<20;i++) if(unitsArray[i].equals(yearList[1])) yNumber += i*100;
	          for(int i=0;i<10;i++) if(tensArray[i].equals(yearList[2])) yNumber += i*10;
	          for(int i=0;i<20;i++) if(unitsArray[i].equals(yearList[3])) yNumber += i;
	          break;
		case 3:
	          for(int i=0;i<10;i++) if(tensArray[i].equals(yearList[0])) yNumber += i*1000;
	          for(int i=0;i<20;i++) if(unitsArray[i].equals(yearList[0])) yNumber += i*100;
	          for(int i=0;i<10;i++) if(tensArray[i].equals(yearList[1])) yNumber += i*10;
	          for(int i=0;i<20;i++) if(unitsArray[i].equals(yearList[1])) yNumber += i*100;
	          for(int i=0;i<10;i++) if(tensArray[i].equals(yearList[2])) yNumber += i*10;
	          for(int i=0;i<20;i++) if(unitsArray[i].equals(yearList[2])) yNumber += i;
	          break;
		case 2:
	          for(int i=0;i<10;i++) if(tensArray[i].equals(yearList[0])) yNumber += i*1000;
	          for(int i=0;i<20;i++) if(unitsArray[i].equals(yearList[0])) yNumber += i*100;
	          for(int i=0;i<10;i++) if(tensArray[i].equals(yearList[1])) yNumber += i*10;
	          for(int i=0;i<20;i++) if(unitsArray[i].equals(yearList[1])) yNumber += i; 
	          break;
		default:
			System.out.println("Nam nhap vao khong dung!");
		}

	  this.year = yNumber;
	}
	
	public int getDay() {
		return day;
	}

	public boolean setDay(int day) {
		if(day<=0 || day >31)
			return false;
		else if(day==31) {
			if(this.month==1 || this.month==3 || this.month==5 || this.month==7 || this.month==8 || this.month==10 || this.month==12) {
				this.day = day;
				return true;
			}else return false;

		}else if(day==30) {
			if(this.month==4 || this.month==6 || this.month==9 || this.month==11) {
				this.day = day;
				return true;
			}else return false;
		}else if(day==29) {
			if(this.month==2 && this.year%4==0) {
				this.day = day;
				return true;
			}else return false;
		}else {
			this.day = day;
			return true;
		}	
	}

	public int getMonth() {
		return month;
	}

	public boolean setMonth(int month) {
		if(this.month<=12 && this.month>=0) {
			this.month = month;
			return true;
		}else return false;
	}

	public int getYear() {
		return year;
	}

	public boolean setYear(int year) {
		if(year >=0 ) {
			this.year = year;
			return true;
		}else return false;
	}
	//Feb 4th 1999
	public void accept() {
		Scanner sc = new Scanner(System.in);
		System.out.println("Nhap date: ");
		String date = sc.nextLine();
		trans(date);
	}
	
	public void trans(String date) {
		String[] parts= date.split(" ");
	    
	    String monthStr = parts[0];
	    String dayStr = parts[1];
	    String yearStr = parts[2];
	    
	    switch(monthStr) {
			case "January": case "Jan": case "1": this.month = 1; break;
			case "March": case "Mar": case "3": this.month = 3; break;
			case "May": case "5": this.month = 5; break;
			case "July": case "Jul": case "7": this.month = 7; break;
			case "August": case "Aug": case "8": this.month = 8; break;
			case "October": case "Oct": case "10": this.month = 10; break;
			case "December": case "Dec": case "12": this.month = 12; break;
			case "February": case "Feb": case "2": this.month = 2; break;
			case "April": case "Apr": case "4": this.month = 4; break;
			case "June": case "Jun": case "6": this.month = 6; break;
			case "September": case "Sept": case "Sep": case "9": this.month = 9; break;
			case "November": case "Nov": case "11": this.month = 11; break;
			default: System.out.println("Thang nhap vao khong dung!"); break;
	    }
	    
	    String dayArr[] = {"1st", "2nd", "3rd", "4th", "5th", "6th", "7th", "8th","9th", "10th", "11th", "12th", "13th", "14th", "15th", "16th", "17th", "18th", "19th", "20th", "21st", "22nd", "23rd", "24th", "25th", "26th", "27th", "28th", "29th", "30th", "31st"};  
	    for(int i=0; i<31; i++) {
	    	if(dayStr.equals(dayArr[i])) {
	    		this.day=i+1;
	    		break;
	    	}
	    	if(i==30) {
	    		System.out.println("Ngay nhap vao khong dung!");
	    	}
	    }
	    
	    if(year>=0) {
	    	this.year = Integer.parseInt(yearStr);
	    }else System.out.println("Nam nhap vao khong dung!");
	}//February 18th 2019
	
//	public void print() {
//		System.out.println("current date -> day: "+cal.get(Calendar.DAY_OF_MONTH)+" month: "+(cal.get(Calendar.MONTH)+1)+" year: "+cal.get(Calendar.YEAR));
//	}
	
	public void print() {
	    String dayArr[] = {"1st", "2nd", "3rd", "4th", "5th", "6th", "7th", "8th","9th", "10th", "11th", "12th", "13th", "14th", "15th", "16th", "17th", "18th", "19th", "20th", "21st", "22nd", "23rd", "24th", "25th", "26th", "27th", "28th", "29th", "30th", "31st"};  
		String monthArr[] = {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
		int dayNumber=cal.get(Calendar.DAY_OF_MONTH);
		int monthNumber=cal.get(Calendar.MONTH)+1;
		System.out.println(monthArr[monthNumber-1]+" "+dayArr[dayNumber-1]+" "+cal.get(Calendar.YEAR)); //February 29th 2020
	}
	
	public void printFormat() {
		Scanner sc = new Scanner(System.in);
		System.out.println("Nhap date: ");
		String date = sc.nextLine();
		String[] parts= date.split(" ");
	    String monthStr = parts[0];
		trans(date);
		System.out.println("Chon format: ");
		System.out.println("1. yyyy-MM-dd");
		System.out.println("2. d/M/yyyy");
		System.out.println("3. dd-MMM-yyyy");
		System.out.println("4. MMM d yyyy");
		System.out.println("5. mm-dd-yyyy");
		int choose;
		do {
			System.out.print("Nhap lua chon: ");
			Scanner sc2 = new Scanner(System.in);
			choose = sc2.nextInt();
			switch(choose) {
			case 1:
				System.out.println(year+"-"+month+"-"+day);
				return;
			case 2:
				System.out.println(day+"/"+month+"/"+year);
				return;
			case 3:
				System.out.println(day+"-"+monthStr+"-"+year);
				return;
			case 4:
				System.out.println(monthStr+" "+day+" "+year);
				return;
			case 5:
				System.out.println(month+"-"+day+"-"+year);
				return;
			default: 
				System.out.println("Chon tu 1 -> 5!");
				break;
			}
		}while(true);
	}
	
}